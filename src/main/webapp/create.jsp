<%-- 
    Document   : create
    Created on : 13-oct-2020, 23:34:26
    Author     : Constance
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html style="height: 100vh">
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Ingresa tu Mascota</title>
    </head>
    <body class="text-center">
        <div class="d-flex flex-row justify-content-center" style="height: 80vh">
            
            <div class="d-flex flex-column">
            <h1 class="text-center">Registra los Datos de tu Mascota</h1>
            
            <form  name="form" action="ListController" method="POST">
                <div class="form-group">
                    <label for="rut">Rut Mascota</label>
                    <input type="text" name="id" class="form-control" required id="id" >
                </div>
                <br>
                <div class="form-group">
                    <label for="chip">¿Tiene Chip?</label>
                    <label>Si tu mascota no tiene chip, deja el campo en blanco</label>
                    <input  type="text" step="any" name="chip" class="form-control" id="chip">
                </div>       
                <br>
                <div class="form-group">
                    <label for="nombre">Nombre de Mascota</label>
                    <input type="text" name="name" class="form-control" required id="name">                 
                </div>        
                <br>
                <div class="form-group">
                    <label for="color">Color Principal del Pelaje</label>
                    <input type="text" name="color" class="form-control" required id="color">                 
                </div>        
                <br>
                <div class="form-group">
                    <label for="type">¿Perro o Gato?</label>
                    <input type="text" name="type" class="form-control" required id="type">                 
                </div>        
                <br>
                <div class="d-flex flex-row justify-content-around my-2">
                    <button type="submit" name="action" value="registerSave" class="btn btn-lg btn-success">Salvar</button>
                    <button type="submit" name="action" value="exit" class="btn btn-lg btn-success">Salir</button>
                </div>
                </form>  
            </div>
        </div>
    </body>
</html>
