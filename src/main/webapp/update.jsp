<%-- 
    Document   : update
    Created on : 14-oct-2020, 2:39:25
    Author     : Constance
--%>

<%@page import="cl.entities.PetsData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
  PetsData pet=(PetsData)request.getAttribute("pet");
%>
<html style="height: 100vh">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Actualiza los Datos de tu Mascota</title>
    </head>
    <body>
        <div class="d-flex flex-column align-items-center">
            <h1>Actualiza los Datos del Registro de tu Mascota</h1>
            
            <form  name="form" action="ListController" method="POST">
                    <div class="form-group">
                        <label for="rut">Rut Mascota</label>
                        <input  name="id" class="form-control"  value="<%= pet.getPetId()%>"  required id="id" >
                           </div>
                    <br>
                    <div class="form-group">
                        <label for="nombre">¿Tiene Chip?</label>
                        <label>Si tu mascota no tiene chip, deja el campo en blanco</label>
                        <input type="text" step="any" name="chip" class="form-control"  value="<%= pet.getPetChip() %>" id="chip">
                     </div>       
                    <br>
                    <div class="form-group">
                        <label for="ranking">Nombre de Mascota</label>
                         <input  name="name" class="form-control"  value="<%= pet.getPetName() %>"   required id="name" >                 
                      </div>        
                    <br>
                    <div class="form-group">
                        <label for="ranking">Color Principal del Pelaje</label>
                         <input  name="color" class="form-control"  value="<%= pet.getPetColor()%>"   required id="color" >                
                      </div>        
                    <br>
                     <div class="form-group">
                        <label for="ranking">¿Perro o Gato?</label>
                         <input  name="type" class="form-control"  value="<%= pet.getPetType()%>"   required id="type" >                
                      </div>        
                    <br>
                    <div class="d-flex flex-row justify-content-around">
                        <button type="submit" name="action" value="updateRegister" class="btn btn-lg btn-success">Actualizar</button>
                        <button type="submit" name="action" value="exit" class="btn btn-lg btn-success">Salir</button>
                    </div>
            </form>
        </div>  
    </body>
</html>