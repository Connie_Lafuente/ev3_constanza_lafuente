<%-- 
    Document   : index
    Created on : 27-oct-2020, 11:37:26
    Author     : Constance
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html style="height: 100vh">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Pet Register</title>
    </head>
    <body>
        <div class="container mt-4">
            <h3>Constanza Lafuente M.</h3>
            <br>
            <h4>Sección 50</h4>
        </div>
     
        
        <div class="d-flex flex-column align-items-center justify-content-center" style="height: 60vh">
            <div class="d-flex align-self-center my-5 mx-5">
                <h1>Bienvenidos al Mundo de las Mascotas</h1>
             
                <div class="container my-5">
                    <form name="form" method="POST" action="InputRegisterController" >

                        <button type="submit" type="submit" name="accion" value="registrar" class="btn btn-lg btn-success">Entrar</button>

                    </form>
                </div>
            </div>
        </div>
        
        <div class="d-flex flex-column justify-content-center">
            <table class="table" style="width: 80%">
                <tbody>
                    <tr>
                        <th>URL</th>
                        <th>Método</th>
                        <th>Significado</th>
                    </tr>
                    <tr>
                        <td>http://desktop-cuiar89:8080/Ev3_Constanza_Lafuente-1.0-SNAPSHOT/api/pets</td>
                        <td>GET</td>
                        <td>Consulta la lista de mascotas</td>
                    </tr
                     <tr>
                        <td>http://desktop-cuiar89:8080/Ev3_Constanza_Lafuente-1.0-SNAPSHOT/api/pets/{idsearch}</td>
                        <td>GET</td>
                        <td>Consulta la lista de mascotas según ID de datos</td>
                    </tr>
                     <tr>
                        <td>http://desktop-cuiar89:8080/Ev3_Constanza_Lafuente-1.0-SNAPSHOT/api/pets</td>
                        <td>POST</td>
                        <td>Agrega datos nuevos a la base de datos</td>
                    </tr>
                     <tr>
                        <td>http://desktop-cuiar89:8080/Ev3_Constanza_Lafuente-1.0-SNAPSHOT/api/pets</td>
                        <td>PUT</td>
                        <td>Actualiza datos de una entrada en particular</td>
                    </tr>
                     <tr>
                        <td>http://desktop-cuiar89:8080/Ev3_Constanza_Lafuente-1.0-SNAPSHOT/api/pets/{iddelete}</td>
                        <td>DELETE</td>
                        <td>Elimina los datos de una entrada en particular según ID</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
