<%-- 
    Document   : register
    Created on : 13-oct-2020, 21:23:33
    Author     : Constance
--%>

<%@page import="cl.entities.PetsData"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<PetsData> pets = (List<PetsData>) request.getAttribute("pets");
    Iterator<PetsData> itPetsData = pets.iterator();
%>
<html style="height: 100vh">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Registro de Mascotas</title>
    </head>
    <body class="text-center">
        <h1 class="mt-4">Bienvenidos al Registro de Mascotas</h1>
        <div class="d-flex justify-content-center">
            <form name="form" action="ListController" method="POST">  
   
                <div class="d-flex flex-column justify-content-center" style="height: 80vh">
                    <table class="table table-hover">
                        <thead>
                        <th>Rut Mascota</th>
                        <th>Nombre Mascota</th>
                        <th>¿Tiene Chip? </th>
                        <th>Color</th>
                        <th>Tipo de Mascota</th>
                        <th> </th>
                        </thead>
                        <tbody>
                            <%while (itPetsData.hasNext()) {
                           PetsData pet = itPetsData.next();%>
                            <tr>
                                <td><%= pet.getPetId()%></td>
                                <td><%= pet.getPetName()%></td>
                                <td><%= pet.getPetChip()%></td>
                                <td><%= pet.getPetColor()%></td>
                                <td><%= pet.getPetType()%></td>

                                <td> <input type="radio" name="selection" value="<%= pet.getPetId()%>"> </td>
                            </tr>
                            <%}%>                
                        </tbody>           
                    </table>
                    <div class="d-flex flex-row justify-content-around my-5">    
                        <button type="submit" name="action" value="edit" class="btn btn-lg btn-success" style="margin-top: 20px;margin-bottom: 50px;">Editar</button>
                        <button type="submit" name="action" value="delete" class="btn btn-lg btn-success" style="margin-top: 20px;margin-bottom: 50px;">Eliminar</button>
                        <button type="submit" name="action"  value="register" class="btn btn-lg btn-success" style="margin-top: 20px;margin-bottom: 50px;">Registrar</button>
                    </div>

            </form>
        </div>             
    </body>
</html>
