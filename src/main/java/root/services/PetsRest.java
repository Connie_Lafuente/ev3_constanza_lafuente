/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import cl.dao.PetsDataJpaController;
import cl.dao.exceptions.NonexistentEntityException;
import cl.entities.PetsData;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Constance
 */
@Path("/pets")
public class PetsRest {
    
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("petDS");
    EntityManager em;
    
    @GET    
    @Produces(MediaType.APPLICATION_JSON)
     public Response listEverything() {
           
       PetsDataJpaController dao = new PetsDataJpaController();
       List<PetsData> list = dao.findPetsDataEntities();
     
        return Response.ok(200).entity(list).build();
    } 
     
     @GET
     @Path("/{idsearch}")
     @Produces(MediaType.APPLICATION_JSON)
     
     public Response searchId(@PathParam("idsearch") String idsearch) {
          PetsDataJpaController dao = new PetsDataJpaController();
          PetsData pet = dao.findPetsData(idsearch);
          return Response.ok(200).entity(pet).build();
     }    
     
     @POST
     @Produces(MediaType.APPLICATION_JSON)
     public String newPet(PetsData pet) {
         PetsDataJpaController dao = new PetsDataJpaController();
         try {
            dao.create(pet);
         } catch (Exception ex) {
             Logger.getLogger(PetsRest.class.getName()).log(Level.SEVERE, null, ex);            
         }
         return "Mascota Guardada";
     }
     
     @PUT
     public Response updatePet(PetsData pet) {
        
        PetsDataJpaController dao = new PetsDataJpaController();
        try {
            dao.edit(pet);
        } catch (Exception ex) {
            Logger.getLogger(PetsRest.class.getName()).log(Level.SEVERE, null, ex);
        }
     
            return Response.ok(200).entity(pet).build();
     }
     
     @DELETE
     @Path("/{iddelete}")
     @Produces(MediaType.APPLICATION_JSON)
     public Response deleteId(@PathParam("iddelete") String iddelete) {
         
        PetsDataJpaController dao = new PetsDataJpaController();
        try {
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(PetsRest.class.getName()).log(Level.SEVERE, null, ex);
        }
     
          return Response.ok("Mascota Eliminada").build();
     }
}
