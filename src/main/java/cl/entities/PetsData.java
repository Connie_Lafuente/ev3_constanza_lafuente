/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Constance
 */
@Entity
@Table(name = "pets_data")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PetsData.findAll", query = "SELECT p FROM PetsData p"),
    @NamedQuery(name = "PetsData.findByPetId", query = "SELECT p FROM PetsData p WHERE p.petId = :petId"),
    @NamedQuery(name = "PetsData.findByPetType", query = "SELECT p FROM PetsData p WHERE p.petType = :petType"),
    @NamedQuery(name = "PetsData.findByPetName", query = "SELECT p FROM PetsData p WHERE p.petName = :petName"),
    @NamedQuery(name = "PetsData.findByPetColor", query = "SELECT p FROM PetsData p WHERE p.petColor = :petColor"),
    @NamedQuery(name = "PetsData.findByPetChip", query = "SELECT p FROM PetsData p WHERE p.petChip = :petChip")})
public class PetsData implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "pet_id")
    private String petId;
    @Size(max = 2147483647)
    @Column(name = "pet_type")
    private String petType;
    @Size(max = 2147483647)
    @Column(name = "pet_name")
    private String petName;
    @Size(max = 2147483647)
    @Column(name = "pet_color")
    private String petColor;
    @Size(max = 2147483647)
    @Column(name = "pet_chip")
    private String petChip;

    public PetsData() {
    }

    public PetsData(String petId) {
        this.petId = petId;
    }

    public String getPetId() {
        return petId;
    }

    public void setPetId(String petId) {
        this.petId = petId;
    }

    public String getPetType() {
        return petType;
    }

    public void setPetType(String petType) {
        this.petType = petType;
    }

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public String getPetColor() {
        return petColor;
    }

    public void setPetColor(String petColor) {
        this.petColor = petColor;
    }

    public String getPetChip() {
        return petChip;
    }

    public void setPetChip(String petChip) {
        this.petChip = petChip;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (petId != null ? petId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PetsData)) {
            return false;
        }
        PetsData other = (PetsData) object;
        if ((this.petId == null && other.petId != null) || (this.petId != null && !this.petId.equals(other.petId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.entities.PetsData[ petId=" + petId + " ]";
    }
    
}
