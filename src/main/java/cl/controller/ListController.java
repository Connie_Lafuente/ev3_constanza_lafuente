/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import cl.entities.PetsData;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import utils.Config;

/**
 *
 * @author Constance
 */
@WebServlet(name = "ListController", urlPatterns = {"/ListController"})
public class ListController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            
       String action = request.getParameter("action");
       
       //Nos lleva a la vista create.jsp
       
       if (action.equals("register")) {
       
           request.getRequestDispatcher("create.jsp").forward(request, response);
       }
       
        //CRUD editar
        if (action.equals("edit")) {
            String selection = request.getParameter("selection");
            
            Config.getEnvironmentVariables();

            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target(Config.host + "/" + selection);
            
            PetsData pet = myResource.request(MediaType.APPLICATION_JSON).get(PetsData.class);
             
            request.setAttribute("pet", pet);
            
            request.getRequestDispatcher("update.jsp").forward(request, response);
       }
        
       //CRUD Delete
       if (action.equals("delete")) {
           String selection = request.getParameter("selection");
           
           Config.getEnvironmentVariables();
           
           Client client1 = ClientBuilder.newClient();
           WebTarget myResource1 = client1.target(Config.host + "/" + selection);
           
           myResource1.request(MediaType.APPLICATION_JSON).delete();
          
           Client client = ClientBuilder.newClient();
           WebTarget myResource = client.target(Config.host);

           List<PetsData> pets = myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<PetsData>>() {
           });

           request.setAttribute("pets", pets);
           request.getRequestDispatcher("register.jsp").forward(request, response);
           
           
       } 
       //CRUD create
       if (action.equals("registerSave")) {
           String id = request.getParameter("id");
           String chip = request.getParameter("chip");
           String name = request.getParameter("name");
           String color = request.getParameter("color");
           String type = request.getParameter("type");
                   
           PetsData createPet = new PetsData();
           
           createPet.setPetId(id);
           createPet.setPetChip(chip);
           createPet.setPetName(name);
           createPet.setPetColor(color);
           createPet.setPetType(type);
           
           Config.getEnvironmentVariables();
         
           Client client1 = ClientBuilder.newClient();
           WebTarget myResource1 = client1.target(Config.host);
           
           myResource1.request(MediaType.APPLICATION_JSON).post(Entity.json(createPet));
          
           
           Client client = ClientBuilder.newClient();
           WebTarget myResource = client.target(Config.host);

           List<PetsData> pets = myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<PetsData>>() {
           });

           request.setAttribute("pets", pets);
           request.getRequestDispatcher("register.jsp").forward(request, response);
       }    
       
       //CRUD update
       if (action.equals("updateRegister")) {
           
           String id = request.getParameter("id");
           String chip = request.getParameter("chip");
           String name = request.getParameter("name");
           String color = request.getParameter("color");
           String type = request.getParameter("type");
           
           PetsData editPet = new PetsData();
           editPet.setPetId(id);
           editPet.setPetChip(chip);
           editPet.setPetName(name);
           editPet.setPetColor(color);
           editPet.setPetType(type);
           
           Config.getEnvironmentVariables();
           
           Client client1 = ClientBuilder.newClient();
           WebTarget myResource1 = client1.target(Config.host);
           myResource1.request(MediaType.APPLICATION_JSON).put(Entity.json(editPet), PetsData.class);

           Client client = ClientBuilder.newClient();
           WebTarget myResource = client.target(Config.host);

           List<PetsData> pet = myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<PetsData>>() {
            });

           request.setAttribute("pets", pet);                    
           request.getRequestDispatcher("register.jsp").forward(request, response);
       }
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
